package config

const(
	AccessToken = "<ACCESS_TOKEN>"
)

const (
	PrivateKeyLocation = "<LOCATION_OF_PRIVATE_KEY_FILE>"
	PrivateKeyPassword = "<PRIVATE_KEY_PASSWORD>"
)

const(
	UrlInitializeSignature = "https://fw2.bry.com.br/api/cms-signature-service/v1/signatures/initialize"
	UrlFinalizeSignature = "https://fw2.bry.com.br/api/cms-signature-service/v1/signatures/finalize"
)

// Available values: "BASIC", "CHAIN", "CHAIN_CRL", "TIMESTAMP", "COMPLETE", "ADRB", "ADRT", "ADRV", "ADRC", "ADRA", "ETSI_B", "ETSI_T", "ETSI_LT" and "ETSI_LTA".
const(
	Basic = "BASIC"
	Chain = "CHAIN"
	ChainCrl = "CHAIN_CRL"
	Timestamp = "TIMESTAMP"
	Complete = "COMPLETE"
	Adrb = "ADRB"
	Adrt = "ADRT"
	Adrv = "ADRV"
	Adrc = "ADRC"
	Adra = "ADRA"
	EtsiB = "ETSI_B"
	EtsiT = "ETSI_T"
	EtsiLT = "ETSI_LT"
	EtsiLTA = "ETSI_LTA"
)

// Available values: "SHA1", "SHA256" e "SHA512".
const(
	Sha1 = "SHA1"
	Sha256 = "SHA256"
	Sha512 = "SHA512"
)

// Available values: "SIGNATURE", "CO_SIGNATURE	" and "COUNTER_SIGNATURE".
const(
	Signature = "SIGNATURE"
	CoSignature = "CO_SIGNATURE"
	CounterSignature = "COUNTER_SIGNATURE"
)

//Request
const (
	HashAlgorithm = Sha256
	Profile = Basic
	Nonce = "1"
	DocumentPath = "./resource/document.txt"
	Attached = "true"
	OperationType = Signature
	BinaryContent = "false"
)