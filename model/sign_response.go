package model

import (
	"strconv"
	"strings"
)

type Content struct {
	Nonce int
	Content string
}

type ResponseInitialization struct {
	SignedAttributes []Content `json:"signedAttributes"`
}

type ResponseFinalization struct {
	Nonce int
	Signatures []Content
}

func (c Content) Report() string {
	sb := strings.Builder{}
	sb.WriteString("\t\t{\n")
	sb.WriteString("\t\tNonce: " + strconv.Itoa(c.Nonce) + "\n")
	sb.WriteString("\t\tContent: " + c.Content + "\n")
	sb.WriteString("\t\t}\n")
	return sb.String()
}

func (rf ResponseFinalization) Report() string{
	sb := strings.Builder{}
	sb.WriteString("Response Finalization {\n")
	sb.WriteString("\tNonce: " + strconv.Itoa(rf.Nonce) + "\n")
	sb.WriteString("\tSignatures: [\n")
	for _, signature := range rf.Signatures {
		sb.WriteString(signature.Report())
	}
	sb.WriteString("\t]\n")
	sb.WriteString("}\n")
	return sb.String()
}

