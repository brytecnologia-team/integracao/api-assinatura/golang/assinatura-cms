package model

type PKCS1Dto struct {
	Nonce int
	Content string

	SignedAttribute string
	SignatureValue string
	InitializedDocument string
	OriginalDocument string
}

