package model

type RequestFinalization struct {
	Nonce string                `json:"nonce"`
	AssinaturasPkcs1 []PKCS1Dto `json:"assinaturasPkcs1"`
	FormatoDeDados string       `json:"formatoDeDados"`
}