package sign

import (
	"assinatura-cms/config"
	"assinatura-cms/mapper"
	"assinatura-cms/model"
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
)

func Initialize(cert model.BryCertificate) *http.Response{
	params := map[string]string {
		"nonce": config.Nonce,
		"profile" : config.Profile,
		"certificate": cert.GetX509Base64(),
		"operationType": config.OperationType,
		"hashAlgorithm": config.HashAlgorithm,
		"attached": config.Attached,
		"originalDocuments[0][nonce]": config.Nonce,
	}
	files := map[string]string {
		"originalDocuments[0][content]": config.DocumentPath,
	}

	req, err := NewFormDataRequest(config.UrlInitializeSignature, params, files)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", config.AccessToken))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	if resp.StatusCode != 200 {
		str, err := mapper.JsonMapper{}.DecodeToString(resp)
		if err != nil {
			log.Fatal(err)
		}
		log.Fatal(str)
	}
	return resp
}

func GetContentInitializationResponse(response *http.Response) []model.PKCS1Dto {
	ri, err := mapper.JsonMapper{}.DecodeToResponseInitialization(response)
	if err != nil {
		log.Fatal(err)
	}
	data, err := mapper.PKCS1Mapper{}.DecodeToPKCS1(*ri)
	if err != nil {
		log.Fatal(err)
	}
	return data
}

func EncryptSignedAttributes(attributes []model.PKCS1Dto, cert model.BryCertificate) {
	if len(attributes) > 0 {
		content, err := base64.StdEncoding.DecodeString(attributes[0].SignedAttribute)
		if err != nil {
			log.Fatal(err)
		}
		attributes[0].SignatureValue = cert.Sign(content)
	}
}

func Finalization(attributes []model.PKCS1Dto, cert model.BryCertificate){
	files := map[string]string{}
	params := map[string]string{
		"nonce": config.Nonce,
		"profile": config.Profile,
		"attached": config.Attached,
		"hashAlgorithm": config.HashAlgorithm,
		"certificate": cert.GetX509Base64(),
		"operationType": config.OperationType,
	}

	for index, attr := range attributes {
		params[ "finalizations["+strconv.Itoa(index)+"][nonce]" ] = strconv.Itoa(attr.Nonce)
		params[ "finalizations["+strconv.Itoa(index)+"][signedAttributes]" ] = attr.SignedAttribute
		params[ "finalizations["+strconv.Itoa(index)+"][signatureValue]" ] = attr.SignatureValue
		files[ "finalizations["+strconv.Itoa(index)+"][document]" ] = attr.OriginalDocument
	}

	req, err := NewFormDataRequest(config.UrlFinalizeSignature, params, files)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", config.AccessToken))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	reportFinalization(resp)
}

func reportFinalization(response *http.Response){
	rf, err := mapper.JsonMapper{}.DecodeToResponseFinalization(response)
	if err != nil {
		log.Fatal(err)
	}

	if len(rf.Signatures) > 0 {
		contentDecoded, err := base64.StdEncoding.DecodeString(rf.Signatures[0].Content)
		if err != nil {
			log.Fatal(err)
		}

		f, err := os.Create("./signature.cms")
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		f.Write(contentDecoded)

		log.Println("CMS signature saved: ./signature.cms")
		log.Println(rf.Report())
	}

}