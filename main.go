package main

import (
	"assinatura-cms/model"
	"assinatura-cms/sign"
	"log"
)

func main(){
	log.Println("Step 1 - Load PrivateKey and Certificate")
	cert := model.LoadCertificate()

	log.Println("Step 2 - Signature initialization.")
	resp := sign.Initialize(cert)
	attributes := sign.GetContentInitializationResponse(resp)

	log.Println("Step 3 - Local encryption of signed attributes using private key.")
	sign.EncryptSignedAttributes(attributes, cert)

	log.Println("Step 4 - Signature finalization.")
	sign.Finalization(attributes, cert)
}
