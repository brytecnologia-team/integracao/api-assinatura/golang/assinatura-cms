package mapper

import (
	"assinatura-cms/config"
	"assinatura-cms/model"
	"errors"
)

type PKCS1Mapper struct {}

func (pkcs1Mapper PKCS1Mapper) DecodeToPKCS1(ri model.ResponseInitialization) ([]model.PKCS1Dto, error){
	data := []model.PKCS1Dto{}
	for _, signature := range ri.SignedAttributes {
		data = append(data, model.PKCS1Dto{Nonce: signature.Nonce, SignedAttribute: signature.Content})
	}
	if len(data) > 0 {
		data[0].OriginalDocument = config.DocumentPath
	} else {
		return data, errors.New("no signed attributes")
	}
	return data, nil
}
